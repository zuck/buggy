# Build environment
FROM node:8 as builder

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
RUN npm install --silent

COPY . /usr/src/app

RUN quasar build -m pwa

# Production environment
FROM nginx:1.13.9-alpine

COPY --from=builder /usr/src/app/dist/pwa-mat /usr/share/nginx/html

EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]
