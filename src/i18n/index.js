import en from './en-us'
import it from './it'

export default {
  'en-us': en,
  it
}
