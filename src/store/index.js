import Vue from 'vue'
import Vuex from 'vuex'
import { Cookies } from 'quasar'

Vue.use(Vuex)

const AUTH_COOKIE_NAME = 'buggy-auth'
const LOCALE_COOKIE_NAME = 'buggy-locale'

const defaultLocale = Cookies.has(LOCALE_COOKIE_NAME) ? Cookies.get(LOCALE_COOKIE_NAME) : 'en-us'

const store = new Vuex.Store({
  state: {
    currentUser: null,
    currentAuthToken: null,
    currentProject: null,
    currentIssue: null,
    projects: [],
    projectCount: 0,
    issues: [],
    issueCount: 0,
    issueSearchQuery: null,
    issueFilterBy: null,
    labels: [],
    issueNotes: [],
    issueNoteCount: 0,
    settings: {
      lang: defaultLocale
    }
  },

  mutations: {
    setLocale (state, langCode) {
      state.settings.lang = langCode || state.settings.lang
      Cookies.set(LOCALE_COOKIE_NAME, state.settings.lang, {
        path: '/'
      })
    },

    setCurrentUser (state, user) {
      state.currentUser = user
    },

    setCurrentAuthToken (state, token) {
      state.currentAuthToken = token
    },

    setCurrentProject (state, project) {
      state.currentProject = project
    },

    setProjectCount (state, count) {
      state.projectCount = count
    },

    setProjects (state, projects) {
      state.projects = projects
    },

    appendProjects (state, projects) {
      state.projects = state.projects.concat(projects)
    },

    setLabels (state, labels) {
      state.labels = labels
    },

    setCurrentIssue (state, issue) {
      state.currentIssue = issue
    },

    setIssueCount (state, count) {
      state.issueCount = count
    },

    setIssues (state, issues) {
      state.issues = issues
    },

    appendIssues (state, issues) {
      state.issues = state.issues.concat(issues)
    },

    setIssueSearchQuery (state, query) {
      state.issueSearchQuery = query
    },

    setIssueFilterBy (state, filterBy) {
      state.issueFilterBy = filterBy
    },

    addIssue (state, issue) {
      state.issues.unshift(issue)
      state.issueCount += 1
    },

    updateIssue (state, issue) {
      if (issue.id) {
        const issueIdx = state.issues.findIndex(item => item.id === issue.id)
        if (issueIdx !== -1) {
          Vue.set(state.issues, issueIdx, Object.assign({}, state.issues[issueIdx], issue))
        }
        if (state.currentIssue && issue.id === state.currentIssue.id) {
          state.currentIssue = Object.assign({}, state.currentIssue, issue)
        }
      }
    },

    removeIssue (state, issue) {
      if (issue && issue.id) {
        const _idx = state.issues.findIndex(item => item.id === issue.id)
        if (_idx > -1) {
          state.issues.splice(_idx, 1)
          state.issueCount -= 1
        }
      }
    },

    setIssueNoteCount (state, count) {
      state.issueNoteCount = count
    },

    setIssueNotes (state, notes) {
      state.issueNotes = notes
    },

    appendIssueNotes (state, notes) {
      state.issueNotes = state.issueNotes.concat(notes)
    },

    addIssueNote (state, note) {
      state.issueNotes.push(note)
      state.issueNoteCount += 1
    },

    updateIssueNote (state, note) {
      if (note.id) {
        const noteIdx = state.issueNotes.findIndex(item => item.id === note.id)
        if (noteIdx !== -1) {
          Vue.set(state.issueNotes, noteIdx, Object.assign({}, state.issueNotes[noteIdx], note))
        }
      }
    }
  },

  actions: {
    login ({ commit }, { token, rememberMe }) {
      token = (token === undefined) ? Cookies.get(AUTH_COOKIE_NAME) : token
      rememberMe = (rememberMe === undefined) ? Cookies.has(AUTH_COOKIE_NAME) : rememberMe
      return Vue.prototype.$gitlab
        .auth(token)
        .then(api => {
          if (rememberMe) {
            Cookies.set(AUTH_COOKIE_NAME, token, {
              path: '/',
              expires: 30
            })
          } else {
            Cookies.remove(AUTH_COOKIE_NAME)
          }
          return api.users.current().then(user => {
            commit('setCurrentAuthToken', token)
            commit('setCurrentUser', user)
            return user
          }).catch(e => {
            commit('setCurrentAuthToken', null)
            commit('setCurrentUser', null)
            Cookies.remove(AUTH_COOKIE_NAME)
            throw e
          })
        }).catch(e => {
          commit('setCurrentAuthToken', null)
          commit('setCurrentUser', null)
          Cookies.remove(AUTH_COOKIE_NAME)
          throw e
        })
    },

    logout ({
      commit
    }) {
      commit('setCurrentAuthToken', null)
      commit('setCurrentUser', null)
      Cookies.remove(AUTH_COOKIE_NAME)
      return Promise.resolve()
    },

    fetchProject ({
      commit,
      state
    }, id) {
      const _cachedProject = state.projects.find(project => `${project.id}` === `${id}`)
      if (_cachedProject) {
        commit('setCurrentProject', _cachedProject)
        return Promise.resolve(_cachedProject)
      } else {
        return Vue.prototype.$gitlab
          .auth(state.currentAuthToken)
          .then(api => {
            return api.projects
              .get(id)
              .then(project => {
                commit('setCurrentProject', project)
                return project
              })
          })
      }
    },

    fetchAllProjects ({
      commit,
      state
    }, {
      sortBy,
      page,
      itemsPerPage
    }) {
      const _page = page || 1
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => {
          return api.projects
            .all(sortBy, _page, itemsPerPage)
            .then(projects => {
              if (_page > 1) {
                commit('appendProjects', projects)
              } else {
                commit('setProjects', projects)
              }
              commit('setProjectCount', Vue.prototype.$gitlab.projectCount)
              return state.projects
            })
        })
    },

    fetchIssuesByProject ({
      commit,
      state
    }, {
      projectId,
      searchQuery,
      filterBy,
      page
    }) {
      const _page = page || 1
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => {
          return api.issues
            .all(projectId, searchQuery, filterBy, _page)
            .then(issues => {
              if (_page > 1) {
                commit('appendIssues', issues)
              } else {
                commit('setIssues', issues)
              }
              commit('setIssueSearchQuery', searchQuery)
              commit('setIssueFilterBy', filterBy)
              commit('setIssueCount', Vue.prototype.$gitlab.issueCount)
              return issues
            })
        })
    },

    fetchLabelsByProject ({
      commit,
      state
    }, projectId) {
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => {
          return api.labels
            .all(projectId)
            .then(labels => {
              commit('setLabels', labels)
              return labels
            })
        })
    },

    fetchIssue ({
      commit,
      state
    }, {
      projectId,
      iid
    }) {
      commit('setCurrentIssue', null)
      const _cachedIssue = state.issues.find(issue => `${issue.iid}` === `${iid}`)
      if (_cachedIssue) {
        commit('setCurrentIssue', _cachedIssue)
        return Promise.resolve(_cachedIssue)
      } else {
        return Vue.prototype.$gitlab
          .auth(state.currentAuthToken)
          .then(api => {
            return api.issues
              .get(projectId, iid)
              .then(issue => {
                commit('setCurrentIssue', issue)
                return issue
              })
          })
      }
    },

    fetchNotesByIssue ({
      commit,
      state
    }, {
      projectId,
      issueIid,
      page
    }) {
      const _page = page || 1
      commit('setIssueNotes', [])
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => {
          return api.notes
            .all(projectId, issueIid, _page)
            .then(notes => {
              if (_page > 1) {
                commit('appendIssueNotes', notes)
              } else {
                commit('setIssueNotes', notes)
              }
              commit('setIssueNoteCount', Vue.prototype.$gitlab.issueNoteCount)
              return notes
            })
        })
    },

    submitIssue ({
      commit,
      state
    }, {
      projectId,
      iid,
      title,
      screenshotURI,
      description,
      labels
    }) {
      if (iid) {
        return Vue.prototype.$gitlab
          .auth(state.currentAuthToken)
          .then(api => {
            return api.issues
              .update(
                projectId,
                iid,
                title,
                screenshotURI,
                description,
                labels
              )
              .then(issue => {
                commit('updateIssue', issue)
                commit('setCurrentIssue', issue)
              })
          })
      } else {
        return Vue.prototype.$gitlab
          .auth(state.currentAuthToken)
          .then(api => {
            return api.issues
              .create(
                projectId,
                title,
                screenshotURI,
                description,
                labels
              )
              .then(issue => {
                commit('addIssue', issue)
              })
          })
      }
    },

    changeIssueStatus ({
      commit,
      state
    }, {
      projectId,
      iid,
      stateEvent
    }) {
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => api.issues.changeStatus(
          projectId,
          iid,
          stateEvent
        ).then(issue => {
          commit('updateIssue', issue)
          if (
            (state.issueFilterBy === 'state=opened' && stateEvent === 'close') ||
            (state.issueFilterBy === 'state=closed' && stateEvent === 'reopen')
          ) {
            commit('removeIssue', issue)
          }
        }))
    },

    uploadAttachment ({
      commit,
      state
    }, {
      projectId,
      data
    }) {
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => api.uploads.create(projectId, data))
    },

    submitIssueNote ({
      commit,
      state
    }, {
      projectId,
      issueIid,
      body
    }) {
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => {
          return api.notes
            .create(
              projectId,
              issueIid,
              body
            )
            .then(note => {
              commit('addIssueNote', note)
            })
        })
    },

    updateIssueNote ({
      commit,
      state
    }, {
      projectId,
      issueIid,
      noteId,
      body
    }) {
      return Vue.prototype.$gitlab
        .auth(state.currentAuthToken)
        .then(api => api.notes.update(
          projectId,
          issueIid,
          noteId,
          body
        ).then(note => {
          commit('updateIssueNote', note)
        }))
    }
  }
})

export default store
