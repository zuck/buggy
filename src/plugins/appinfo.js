import * as INFO from '../../package.json'

export default ({ Vue }) => {
  // Set i18n instance on app
  Vue.prototype.$appinfo = {
    name: INFO.name,
    productName: INFO.productName,
    description: INFO.description,
    version: INFO.version,
    license: INFO.license,
    authorName: INFO.author.split('<')[0].trim(),
    authorEmail:
      INFO.email ||
      INFO.author.split('<')[1].replace('>', '').trim(),
    githubURL: INFO.repository ? INFO.repository.url : null
  }
}
