import showdown from 'showdown'
import TurndownService from 'turndown'
import {
  gfm,
  tables
} from 'turndown-plugin-gfm'

const mdToHtmlConverter = new showdown.Converter({
  simplifiedAutoLink: true,
  strikethrough: true,
  tables: true,
  tasklists: true,
  simpleLineBreaks: true
})
mdToHtmlConverter.setFlavor('github')

const markdownToHtml = (md) => mdToHtmlConverter.makeHtml(md)

const htmlToMdConverter = new TurndownService({
  defaultReplacement: (innerHTML, node) => (node.isBlock ? '\n\n' + node.outerHTML + '\n\n' : node.outerHTML)
})
htmlToMdConverter.use(gfm)
htmlToMdConverter.use(tables)

const htmlToMarkdown = (html) => htmlToMdConverter.turndown(html)

const parseScreenshot = (content) => {
  const _content = content || ''
  const index = _content.indexOf('\n\n')
  const left = _content.substr(0, index) || null
  const right = _content.substr(index + 1) || null
  if (left && (left.startsWith('![') || left.startsWith('/'))) {
    return left
  } else if (right && (right.startsWith('![') || right.startsWith('/'))) {
    return right
  }
  return null
}

const parseIssue = (issue) => {
  const re = /^(!|)(\[.*\]|)(\(|)(.*?)\)?$/
  const screenshotURI = parseScreenshot(issue.description)
  const description = markdownToHtml(issue.description.replace(screenshotURI, ''))
  return Object.assign({}, issue, {
    screenshotURI: screenshotURI ? screenshotURI.replace(re, '$4') : null,
    description
  })
}

export default ({
  Vue
}) => {
  // Set i18n instance on app
  const gitLabClient = {
    base_url: 'https://gitlab.com/api/v4',
    privateToken: null,
    itemsPerPage: 30,
    projectCount: 0,
    issueCount: 0,
    issueNoteCount: 0,

    apiURL (url) {
      return [this.base_url, url].join('/')
    },

    get (url, params) {
      return Vue.prototype.$axios.get(this.apiURL(url), {
        params: Object.assign({
          private_token: this.privateToken,
          per_page: this.itemsPerPage
        }, params)
      })
    },

    post (url, params, headers) {
      return Vue.prototype.$axios.post(this.apiURL(url), params, {
        headers: headers || {},
        params: {
          private_token: this.privateToken
        }
      })
    },

    put (url, params, headers) {
      return Vue.prototype.$axios.put(this.apiURL(url), params, {
        headers: headers || {},
        params: {
          private_token: this.privateToken
        }
      })
    },

    auth (token) {
      this.privateToken = token

      return Promise.resolve({
        users: {
          current: () => {
            return this
              .get('user')
              .then(response => response.data || response)
          }
        },

        projects: {
          all: (sortBy, page, itemsPerPage) => {
            return this
              .get('projects/', {
                order_by: sortBy,
                page: page || 1,
                per_page: itemsPerPage || this.itemsPerPage
              })
              .then(response => {
                this.projectCount = Number(response.headers['x-total'])
                return response.data
              })
          },
          get: (id) => {
            return this
              .get('projects/' + id)
              .then(response => response.data || response)
          }
        },

        issues: {
          all: (projectId, searchQuery, filterBy, page) => {
            const _apiURLParams = []
            const _baseApiURL = 'projects/' + projectId + '/issues'
            if (filterBy) {
              _apiURLParams.push(filterBy)
            }
            if (searchQuery) {
              _apiURLParams.push(`search="${searchQuery}"`)
            }
            const _apiURL = _baseApiURL + (_apiURLParams ? '?' + _apiURLParams.join('&') : '')
            return this
              .get(_apiURL, {
                page: page || 1
              })
              .then(response => {
                this.issueCount = Number(response.headers['x-total'])
                return response.data
              })
              .then(issues => {
                return issues.map(issue => parseIssue(issue))
              })
          },
          get: (projectId, iid) => {
            return this
              .get('projects/' + projectId + '/issues/' + iid)
              .then(response => response.data || response)
              .then(issue => parseIssue(issue))
          },
          create: (projectId, title, screenshotURI, description, labels) => {
            const md = [screenshotURI, htmlToMarkdown(description || '')].join('\n\n')
            return this
              .post('projects/' + projectId + '/issues', {
                title,
                description: md,
                labels: (labels || []).join(',')
              })
              .then(response => response.data || response)
              .then(issue => parseIssue(issue))
          },
          update: (projectId, iid, title, screenshotURI, description, labels, stateEvent) => {
            const md = [screenshotURI, htmlToMarkdown(description || '')].join('\n\n')
            return this
              .put('projects/' + projectId + '/issues/' + iid, {
                title,
                description: md,
                labels: (labels || []).join(','),
                state_event: stateEvent
              })
              .then(response => response.data || response)
              .then(issue => parseIssue(issue))
          },
          changeStatus: (projectId, iid, stateEvent) => {
            return this
              .put('projects/' + projectId + '/issues/' + iid, {
                state_event: stateEvent
              })
              .then(response => response.data || response)
              .then(issue => parseIssue(issue))
          }
        },

        labels: {
          all: (projectId) => {
            return this
              .get('projects/' + projectId + '/labels')
              .then(response => response.data || response)
          }
        },

        notes: {
          all: (projectId, issueIid, page) => {
            return this
              .get('projects/' + projectId + '/issues/' + issueIid + '/notes?sort=asc&order_by=created_at', {
                page: page || 1
              })
              .then(response => {
                this.issueNoteCount = Number(response.headers['x-total'])
                return response.data
              })
              .then(notes => notes.map(
                note => Object.assign({}, note, {
                  body: markdownToHtml(note.body)
                })
              ))
          },
          create: (projectId, issueIid, body) => {
            return this
              .post('projects/' + projectId + '/issues/' + issueIid + '/notes', {
                body: htmlToMarkdown(body)
              })
              .then(response => response.data || response)
              .then(note => Object.assign({}, note, {
                body
              }))
          },
          update: (projectId, issueIid, id, body) => {
            return this
              .put('projects/' + projectId + '/issues/' + issueIid + '/notes/' + id, {
                body: htmlToMarkdown(body)
              })
              .then(response => response.data || response)
              .then(note => Object.assign({}, note, {
                body
              }))
          }
        },

        uploads: {
          create: (projectId, data) => {
            const formData = new FormData()
            formData.append('file', data)
            return this
              .post('projects/' + projectId + '/uploads', formData, {
                'Content-Type': 'multipart/form-data'
              })
              .then(response => response.data || response)
          }
        }
      })
    }
  }

  Vue.prototype.$gitlab = gitLabClient
}
