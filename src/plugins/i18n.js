import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

export default ({ Vue, app }) => {
  Vue.use(VueI18n)

  const defaultLocale = 'en-us'

  const i18n = new VueI18n({
    locale: defaultLocale,
    fallbackLocale: defaultLocale,
    messages
  })

  // Set i18n instance on app
  app.i18n = i18n
}
