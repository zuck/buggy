import { AddressbarColor } from 'quasar'

export default () => {
  AddressbarColor.set('#21a741')
}
