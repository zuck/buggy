
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/index') }
    ]
  },

  {
    path: '/monitor',
    component: () => import('layouts/monitor'),
    children: [
      { path: '', component: () => import('pages/monitor') }
    ]
  },

  {
    path: '/:projectId([\\s\\w\\d\\-]+)',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/project') }
    ]
  },

  {
    path: '/:projectId([\\s\\w\\d\\-]+)/:issueIid([\\s\\w\\d\\-]+)',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/project') }
    ]
  },

  {
    path: '/monitor',
    component: () => import('layouts/monitor'),
    children: [
      { path: '', component: () => import('pages/monitor') }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
