# Buggy

> Bug reporting made simple

## How to install

```bash
$ git clone git@gitlab.com:zuck/buggy.git
$ cd buggy
$ npm install
$ npm install -g quasar-cli
```

## How to test

```bash
$ npm test
```

## How to run locally

```bash
$ quasar dev

# Now connect to: http://localhost:8080
```

## Have you found a bug?

LOL! :)

BTW, please open an issue at:

https://gitlab.com/zuck/buggy/issues

## Authors

- Emanuele Bertoldi

## License

[MIT License](LICENSE)
